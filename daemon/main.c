#include <stdio.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>

#define BUFSIZE 1024*1024


struct pollfd* fds;
int poll_sockets = 1;
int *data;

struct pollfd* create_poll_selector(int sock){
    struct pollfd *fds;
    fds = malloc(sizeof(struct pollfd));
    fds[0].fd = sock;
    fds[0].events = POLLIN;
    return fds;
}

int create_sock(int bport, char* baddr){
    int sock;
    struct sockaddr_in6 addr;

    addr.sin6_family = AF_INET6;
    inet_pton(AF_INET6, baddr, &addr.sin6_addr);
    addr.sin6_port = htons(bport);

    sock = socket(AF_INET6, SOCK_STREAM, 0);
    if (sock == -1){
        printf("err: socket create\n");
    }
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));

    int bnd = bind(sock, (struct sockaddr*)&addr, sizeof(addr));
    if (bnd == -1){
        printf("err: socket bind\n");
    }
    int r = listen(sock, 0);
    if (r == -1){
        printf("err: socket listen\n");
    }
    return sock;
}


int regist_in(int sock){
    poll_sockets++;
    fds = realloc(fds, sizeof(struct pollfd)*(poll_sockets));
    fds[poll_sockets-1].fd = sock;
    fds[poll_sockets-1].events = POLLIN;
    fds[poll_sockets-1].revents = 0;

    data = realloc(data, sizeof(int)*(poll_sockets));
    data[poll_sockets-1] = 1; // check mode
    return 0;
}

int unregist(int i){
    poll_sockets--;
    shutdown(fds[i].fd, SHUT_RDWR);
    if (fds[i].fd != fds[poll_sockets].fd){

        fds[i].fd = fds[poll_sockets].fd;
        fds[i].events = fds[poll_sockets].events;

        data[i] = data[poll_sockets];

    }

    fds = realloc(fds, sizeof(struct pollfd)*(poll_sockets));
    data = realloc(data, sizeof(int)*poll_sockets);

    return 0;
}

int handle_conncetion(){
    struct sockaddr_in cliaddr;
    int addrlen = sizeof(cliaddr);
    int sock = accept(fds[0].fd, (struct sockaddr*)&cliaddr, &addrlen);
    regist_in(sock);

    printf("poll_sockets: %d\n", poll_sockets);
    return 0;
}



int mode_check(int i){
    char *buf[5] = {};
    int n = recv(fds[i].fd, buf, 5, 0);
    if (n != 0){
        if (strcmp(buf, "junqn") == 0){
            printf("%s\n", "junqn connected");
            data[i] = 2;
        } else if (strcmp(buf, "junqc") == 0) {
            data[i] = 3;
            printf("%s\n", "junqc connected");
        } else {
            printf("%s\n", "mode not defined");
            unregist(i);
        }
        return 0;
    } else {
        unregist(i);
    }
    return 1;
}

int close_all(){
    for (int j = 0; j <= poll_sockets; j++){
        unregist(j);
    }
    return 0;
}

int loop(){
    data = malloc(sizeof(int)*poll_sockets);
    int r;

    while (poll_sockets > 0) {
        int ret = poll(fds, poll_sockets, -1);
        if (ret == -1){
            printf("%s\n","err poll!");
        } else if (ret == 0) {
            printf("%s\n","timeout poll!");
        } else {

            for (int i=0; i < poll_sockets; i++){

                if (fds[i].revents & POLLIN){

                    switch (data[i]) {
                        case 0: { // connection
                            handle_conncetion();
                            break;

                        } case 1: { // mode check
                            mode_check(i);
                            break;

                        } case 2: { // junq client data
                            char buf[BUFSIZE] = {0};
                            recv(fds[i].fd, buf, BUFSIZE, 0);
                            break;

                        } case 3: { // junq node data
                            char buf[BUFSIZE] = {0};
                            recv(fds[i].fd, buf, BUFSIZE, 0);
                            break;
                        } 
                        
                        
                        default: {
                            char *buf[4] = {};
                            recv(fds[i].fd, buf, 4, 0);
                            if (strcmp(buf, "exit") == 0){
                                printf("%s\n", "exit");
                                close_all();
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
    return 0;
}

int main(){
    int sock = create_sock(1109, "::1");
    fds = create_poll_selector(sock);
    loop();
}