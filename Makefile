CC = gcc

SRC=daemon/*.c

all: junq-daemon

junq-daemon: $(SRC)
	$(CC) $(SRC) -o junq-daemon